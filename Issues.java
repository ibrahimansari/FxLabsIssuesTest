import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Issue;

import java.util.List;

public class Issues {
    private static GitLabApi gitLabApi = new GitLabApi("https://gitlab.com", "FqTfoyWN9YsqYpaAMYNe");
    private static Integer projectId = new Integer(8199631);
    private static List<Issue> issues;

    public static void main(String[] args) {
        try {
            // First Print Existing Issues to Console
            printIssues();
            // Create new Issue and print new List of Issues to Console
            Integer newID = createIssue("Test Issue", "This is a test issue");
            // Close newly created Issue and print to Console.
            closeIssue(newID);
        } catch (GitLabApiException e) {
            e.printStackTrace();
        }
    }

    private static void printIssues() throws GitLabApiException {
        issues = gitLabApi.getIssuesApi().getIssues(projectId);
        System.out.println("Existing Issues in Repo:");
        for (Issue issue : issues)
            System.out.println(issue.getTitle());
    }

    private static Integer createIssue(String title, String desc) throws GitLabApiException {
        Integer newIssue = gitLabApi.getIssuesApi().createIssue(projectId, title, desc).getIid();
        printIssues();
        return newIssue;
    }

    private static void closeIssue(Integer issueId) throws GitLabApiException {
        gitLabApi.getIssuesApi().closeIssue(projectId, issueId);
    }
}
